(function($, window, document, Drupal) {
	// $('.moderation__link--delete-draft a').on('click', function() {
	// 	return confirm('Are you sure you want to reset your changes? This will revert the page back to the published version. You will not be able to undo this change.');
	// });

	Drupal.behaviors.VbContentModeration = {
		attach: function (context, settings) {
			$('.moderation__help').off('click.modal').on('click.modal', function() {
				bootbox.alert({
					className: 'content-moderation',
					title: 'This page uses the draft mode functionality',
					message: '<p>Draft mode allows you to work on pages without those changes being visible to the website visitors.</p><p><a href="#" class="btn btn-primary">Documentation lorem ipsum</a></p>',
					buttons: {
						ok: {
							label: 'Close',
							className: 'btn-secondary'
						},
					},
				});
				return false;
			});
			$('.moderation__link--save-draft a').off('click.modal').on('click.modal', function() {
				var link = $(this);
				bootbox.confirm({
					className: 'content-moderation',
					title: "Are you sure you want to publish your changes?",
					message: "This will make your changes visible to the visitors of this page.",
					buttons: {
						confirm: {
							label: '<i class="fa fa-check"></i> Publish',
							className: 'btn-success'
						},
						cancel: {
							label: 'Cancel',
							className: 'btn-secondary'
						}
					},
					callback: function (result) {
						if(result) {
							window.location = link.attr('href');
						}
					}
				});
				return false;
			});
			$('.moderation__link--delete-draft a').off('click.modal').on('click.modal', function() {
				var link = $(this);
				bootbox.confirm({
					className: 'content-moderation',
					title: "Are you sure you want to reset your changes?",
					message: "This will revert the page back to the published version. You will not be able to undo this change.",
					buttons: {
						confirm: {
							label: '<i class="fa fa-times"></i> Reset changes',
							className: 'btn-danger'
						},
						cancel: {
							label: 'Cancel',
							className: 'btn-secondary'
						}
					},
					callback: function (result) {
						if(result) {
							window.location = link.attr('href');
						}
					}
				});
				return false;
			});
		}
	}
})(jQuery, window, document, Drupal);