<?php

namespace Drupal\vb_content_moderation;

use Drupal\entity_reference_revisions\EntityReferenceRevisionsFieldItemList;
use Drupal\paragraphs\ParagraphInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\content_moderation\ModerationInformationInterface;
use Drupal\Core\Session\AccountInterface;


/**
 * DraftOperations class.
 */
class DraftOperations {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The entity field manager.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected $entityFieldManager;

  /**
   * The language manager.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface;
   */
  protected $languageManager;

  /**
   * The Moderation Information service.
   *
   * @var \Drupal\content_moderation\ModerationInformationInterface
   */
  protected $moderationInfo;

  /**
   * The current user account.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $currentUser;


  /**
   * Construct a new DraftOperations object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   Provides EntityTypeManagerInterface service.
   */
  public function __construct(EntityTypeManagerInterface $entityTypeManager, EntityFieldManagerInterface $entityFieldManager, LanguageManagerInterface $languageManager, ModerationInformationInterface $moderationInfo, AccountInterface $currentUser) {
    $this->entityTypeManager = $entityTypeManager;
    $this->entityFieldManager = $entityFieldManager;
    $this->languageManager = $languageManager;
    $this->moderationInfo = $moderationInfo;
    $this->currentUser = $currentUser;
  }

  protected function getLangcode() {
    return $this->languageManager->getCurrentLanguage()->getId();
  }


  /**
   * Check if the passed revision is the live version of the entity
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   The entity whose lineage to save as new revisions.
   */
  public function isLiveVersion(ContentEntityInterface $entity, $revisionId) {
    if(!$this->moderationInfo->isModeratedEntity($entity)) {
      return true;
    }

    $langcode = $this->getLangcode();

    // if($entity->hasTranslation($langcode)) {
    //   $entity = $entity->getTranslation($langcode);
    // }

    kint($entity->getRevisionId());
    kint($revisionId);

    return ($entity->getRevisionId() == $revisionId);
  }


  /**
   * Check if the entity has a draft revision
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   The entity whose lineage to save as new revisions.
   */
  public function hasDraft(ContentEntityInterface $entity) {
    if(!$this->moderationInfo->isModeratedEntity($entity)) {
      return false;
    }
    if(!$this->currentUser->hasPermission('view draft')) {
      return false;
    }

    $storage = $this->entityTypeManager->getStorage($entity->getEntityTypeId());
    $langcode = $this->getLangcode();

    if($entity->hasTranslation($langcode)) {
      $entity = $entity->getTranslation($langcode);
    }

    $latest_revision = $storage->getLatestTranslationAffectedRevisionId($entity->id(), $langcode);
    $latest = $storage->loadRevision($latest_revision);
    if($latest && $latest->hasTranslation($langcode)) {
      $latest = $latest->getTranslation($langcode);
      return ($latest->get('moderation_state')->value == 'draft');
    }

    return false;
  }


  /**
   * Check if the entity only a draft revision
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   The entity whose lineage to save as new revisions.
   */
  public function hasOnlyDraft(ContentEntityInterface $entity) {
    if (!$this->moderationInfo->isModeratedEntity($entity)) {
      return false;
    }
    if(!$this->currentUser->hasPermission('view draft')) {
      return false;
    }

    $storage = $this->entityTypeManager->getStorage($entity->getEntityTypeId());
    $revisions = $storage->revisionIds($entity);
    $langcode = $this->getLangcode();

    // Loop over all revisions of the entity and check if it has
    // any published revisions in the current language
    foreach($revisions as $revision_id) {
      $revision = $storage->loadRevision($revision_id);
      if($revision->hasTranslation($langcode)) {
        $revision = $revision->getTranslation($langcode);
      } else {
        continue;
      }
      if($revision->get('moderation_state')->value == 'published') {
        // Found a published revision
        return false;
      }
    }

    // No published revisions to be found
    return true;
  }

  /**
   * Create a new draft for the given entity
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   The entity whose lineage to save as new revisions.
   */
  private function createDraft(ContentEntityInterface $entity) {
    if (!$this->moderationInfo->isModeratedEntity($entity)) {
      return $entity;
    }
    if(!$this->currentUser->hasPermission('create draft')) {
      return $entity;
    }

    $storage = $this->entityTypeManager->getStorage($entity->getEntityTypeId());
    $langcode = $this->getLangcode();
    $languages = \Drupal::languageManager()->getLanguages();

    $entity = $storage->createRevision($entity);
    $entity->setRevisionCreationTime(\Drupal::time()->getRequestTime());
    $entity->setRevisionUserId(\Drupal::currentUser()->id());
    $entity->setRevisionLogMessage('Automatic draft created by paragraphs builder');
    //$entity->setRevisionTranslationAffected(FALSE);
    $entity->set('moderation_state', 'draft');
    $entity->set('langcode', $langcode);

    return $entity;
  }

  /**
   * Get the draft revision for the given entity
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   The entity whose lineage to save as new revisions.
   */
  public function getDraft(ContentEntityInterface $entity) {
    if (!$this->moderationInfo->isModeratedEntity($entity)) {
      return $entity;
    }
    if(!$this->currentUser->hasPermission('view draft')) {
      return $entity;
    }

    $storage = $this->entityTypeManager->getStorage($entity->getEntityTypeId());
    $langcode = $this->getLangcode();

    // Load the latest revision for given entity
    $vid = $storage->getLatestTranslationAffectedRevisionId($entity->id(), $langcode);
    $entity = $storage->loadRevision($vid);

    // Translate entity
    if($entity->hasTranslation($langcode)) {
      $entity = $entity->getTranslation($langcode);
    }

    // Create draft revision if latest revision isn't one already
    if($entity->get('moderation_state')->value == 'published') {
      $entity = $this->createDraft($entity);
    }

    return $entity;
  }

  /**
   * Publish draft version
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   The entity whose lineage to save as draft.
   */
  public function publishDraft(ContentEntityInterface $entity) {
    if (!$this->moderationInfo->isModeratedEntity($entity)) {
      return;
    }
    if(!$this->currentUser->hasPermission('publish draft')) {
      return;
    }

    $storage = $this->entityTypeManager->getStorage($entity->getEntityTypeId());

    if($this->hasDraft($entity)) {
      $entity = $this->getDraft($entity);
      $entity = $storage->createRevision($entity);
      $entity->setRevisionCreationTime(\Drupal::time()->getRequestTime());
      $entity->setRevisionUserId(\Drupal::currentUser()->id());

      $langcode = $this->getLangcode();
      $languages = \Drupal::languageManager()->getLanguages();

      $entity->set('moderation_state', 'published');
      //$entity->set('langcode', $langcode);
      //$entity->setRevisionTranslationAffected(TRUE);

      // We change the current draft revision to published rather than creating a new one with setSyncing
      $entity->setSyncing(TRUE);
      $entity->save();


      foreach($languages as $language) {
        if($language->getId() != $langcode && $entity->hasTranslation($language->getId())) {
          $translated_entity = $entity->getTranslation($language->getId());
          $translated_entity->setRevisionTranslationAffected(FALSE);
          $translated_entity->save();
        }
      }
    }

    return $entity;
  }

  /**
   * Cancel draft version
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   The entity whose lineage to save as draft.
   */
  public function cancelDraft(ContentEntityInterface $entity) {
    if (!$this->moderationInfo->isModeratedEntity($entity)) {
      return;
    }
    if(!$this->currentUser->hasPermission('cancel draft')) {
      return;
    }

    if($this->hasDraft($entity)) {
      $draft = $this->getDraft($entity);

      // Check for entity_reference_revisions field because we need to
      // revert the referenced entities default revisions too
      $fields = $this->entityFieldManager->getFieldDefinitions($entity->getEntityTypeId(), $entity->bundle());
      foreach($fields as $field_name => $field_definition) {
        if($field_definition->getType() == 'entity_reference_revisions') {
          $this->revertParagraphs($entity, $field_name);
        }
      }

      // Delete draft entity revision
      $storage = $this->entityTypeManager->getStorage($entity->getEntityTypeId());
      $storage->deleteRevision($draft->getRevisionId());
      \Drupal::messenger()->addStatus('Draft has been removed.');
    }

    return $entity;
  }

  /**
   * Revert paragraph default revisions to their previous ones after cancelling a draft
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   The entity whose lineage to save as draft.
   */
  private function revertParagraphs(ContentEntityInterface $entity, $field_name) {
    $storage = $this->entityTypeManager->getStorage('paragraph');
    $field = $entity->get($field_name)->getValue();

    foreach($field as $index => $value) {
      $paragraph = $storage->loadRevision($value['target_revision_id']);
      $paragraph->isDefaultRevision(TRUE);
      $paragraph->save();
    }
  }
}
