<?php

namespace Drupal\vb_content_moderation\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Url;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Controller for up and down actions.
 */
class DraftController extends ControllerBase {

  /**
   * Publish draft version of parent node
   */
  public function publishDraft($node) {
    \Drupal::service('vb_content_moderation.draft_operations')->publishDraft($node);
    \Drupal::messenger()->addStatus('Draft has been published.');
    $url = Url::fromRoute('entity.node.canonical', ['node' => $node->id()])->toString();
    return new RedirectResponse($url);
  }

  /**
   * Cancel draft version of parent node
   */
  public function deleteDraft($node) {
    \Drupal::service('vb_content_moderation.draft_operations')->cancelDraft($node);
    $url = Url::fromRoute('entity.node.canonical', ['node' => $node->id()])->toString();
    return new RedirectResponse($url);
  }

  public function viewLiveTitle($node) {
    return $node->label() . ' (live version)';
  }

  /**
   * Render live version of the page
   */
  public function viewLive($node) {
    // Build the page
    $view_builder = $this->entityTypeManager->getViewBuilder('node');
    $build = $view_builder->view($node);

    // But set paragraphs display options to default instead of vb_paragraphs builder
    $display_options = [
      'label' => 'hidden',
      'type' => 'entity_reference_revisions_entity_view',
      'settings' => [
        'view_mode' => 'default',
      ],
    ];
    $build['field_paragraphs'] = $node->field_paragraphs->view($display_options);

    return $build;
  }
}