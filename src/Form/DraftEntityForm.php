<?php

namespace Drupal\vb_content_moderation\Form;

use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Form\FormBuilderInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Url;
use Drupal\vb_paragraphs\VbParagraphsFormTrait;
use Drupal\Core\Render\Element;
use Drupal\field_group\FormatterHelper;
use stdClass;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\ReplaceCommand;
use Drupal\Core\Ajax\InvokeCommand;
use Drupal\Core\Ajax\CloseDialogCommand;
use Drupal\Core\Ajax\OpenModalDialogCommand;

use Drupal\Core\DrupalKernel;
use Symfony\Component\HttpFoundation\Request;

/**
 * VbParagraphsEdit class.
 */
class DraftEntityForm extends ContentEntityForm {
  use VbParagraphsFormTrait;

  /**
   * {@inheritdoc}
   */
  protected function init(FormStateInterface $form_state) {
    parent::init($form_state);
  }

  public function processForm($element, FormStateInterface $form_state, $form) {

    // If the form is cached, process callbacks may not have a valid reference
    // to the entity object, hence we must restore it.
    $this->entity = $form_state
      ->getFormObject()
      ->getEntity();
    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);

    $node = $form_state->getFormObject()->getEntity();
    $form['#attributes']['class'][] = 'moderation-form';

    // $form['actions']['submit']['#ajax'] = [
    //   'callback' => ['this', 'refreshParagraphsCallback'],
    //   'effect' => 'fade',
    // ];
    $form['#disable_inline_form_errors'] = FALSE;

    // Unset fields that we don't want to show
    unset($form['moderation_state']);
    unset($form['revision_log']);
    unset($form['alias_keeper']);
    unset($form['created']);
    unset($form['node_keeper']);
    unset($form['created']);
    unset($form['revision']);
    unset($form['uid']);


    // Create fieldgroups, if they don't exist already
    $default_tabs = [
      'content' => [
        'id' => 'group_content',
        'label' => 'Content',
        'weight' => -10,
        'formatter' => 'open'
      ],
      'teaser' => [
        'id' => 'group_teaser',
        'label' => 'Teaser',
        'weight' => -9,
        'formatter' => 'closed'
      ],
      'banner' => [
        'id' => 'group_banner',
        'label' => 'Banner',
        'weight' => -8,
        'formatter' => 'closed'
      ],
      'tabs' => [
        'id' => 'group_tabs',
        'label' => 'Tabs',
        'weight' => 0,
        'formatter' => 'closed'
      ],
    ];
    $default_fields = [
      'title' => 'content_tab',
      'langcode' => 'content_tab',
      'field_media_image' => 'teaser_tab',
      'field_summary' => 'teaser_tab',
      'field_banner' => 'banner_tab',
      'field_metatags' => 'vertical_tabs',
      'options' => 'vertical_tabs',
      'path' => 'vertical_tabs',
      'url_redirects' => 'vertical_tabs',
      'scheduler_settings' => 'vertical_tabs'
    ];

    $form['vertical_tabs'] = [
      '#type' => 'vertical_tabs',
      '#default_tab' => 'edit-content',
    ];
    $form['content_tab'] = [
      '#type' => 'details',
      '#title' => $this->t('Content'),
      '#group' => 'vertical_tabs',
    ];
    $form['teaser_tab'] = [
      '#type' => 'details',
      '#title' => $this->t('Teaser'),
      '#group' => 'vertical_tabs',
    ];
    $form['banner_tab'] = [
      '#type' => 'details',
      '#title' => $this->t('Banner'),
      '#group' => 'vertical_tabs',
    ];

    //ksm($form);

    // $push = [];    
    // foreach (Element::children($form) as $key) {
    //   if(!empty($form[$key]['widget'])) {
    //     if(!empty($default_fields[$key])) {
    //       $push[$default_fields[$key]][] = $key;
    //     } else {
    //       $push['content_tab'][] = $key;
    //     }
    //   }
    // }


    // foreach($push as $tab => $fields) {
    //   foreach($fields as $field) {
    //     $form[$tab][$field] = $form[$field];
    //     unset($form[$field]);
    //   }
    // }

    // Unset the advanced tab
    unset($form['advanced']);

    // Change the form actions because we are in a modal
    unset($form['actions']['delete']);
    unset($form['actions']['preview']);
    $form['actions']['cancel'] = [
      '#type' => 'button',
      '#name' => 'cancel',
      '#value' => t('Cancel'),
      '#attributes' => [
        'data-dismiss' => 'modal',
        'class' => [
          'ui-dialog-titlebar-close',
          'btn',
          'btn-info'
        ]
      ]
    ];
    // $form['actions']['submit']['#ajax'] = [
    //   'callback' => [$this, 'refreshPageCallback'],
    //   'effect' => 'fade',
    // ];

    // $form['#title'] = $this->t('Edit @lineage', [
    //   '@lineage' => \Drupal::service('vb_paragraphs.lineage.inspector')->getLineageString($this->entity),
    // ]);

    // $url = Url::fromRoute('vb_paragraphs.edit', [
    //   'root_parent_type' => $form_state->getformObject()->getEntity()->getParentEntity()->getEntityTypeId(),
    //   'root_parent' => $form_state->getformObject()->getEntity()->getParentEntity()->id(),
    //   'paragraph' => $form_state->getformObject()->getEntity()->id()
    // ]);

    // $form['actions']['submit']['#ajax'] = [
    //   'callback' => [$this, 'refreshParagraphsCallback'],
    //   'effect' => 'fade',
    // ];

    // $form['actions']['cancel'] = [
    //   '#type' => 'button',
    //   '#name' => 'cancel',
    //   '#value' => $this->t('Cancel'),
    //   '#attributes' => [
    //     'class' => [
    //       'ui-dialog-titlebar-close',
    //       'btn',
    //       'btn-info'
    //     ]
    //   ]
    // ];

    // $form['actions']['cancel']['#attributes']['data-dismiss'] = 'modal';

    // // Utility function to set correct AJAX url because it gets bugged after consecutive calls
    // $form = $this->recursiveAddAjaxUrl($form, $url);
    
    // $form['#disable_inline_form_errors'] = FALSE;
    // $form['#attributes']['class'][] = 'vb-paragraphs-form';
    // $form['#attached']['library'][] = 'vb_paragraphs/admin';

    // if(!\Drupal::service('vb_paragraphs.utility')->getConfig()->get('enable_title_icon')) {
    //   $form['field_title_icon']['#access'] = FALSE;
    // }
    $form['#process'][] = [FormatterHelper::class, 'formProcess'];
    $form['#pre_render'][] = [FormatterHelper::class, 'formGroupPreRender'];

    return $form;
  }

  /**
   * AJAX callback function to refresh paragraphs.
   */
  public function refreshPageCallback(&$form, FormStateInterface $form_state) {
    ksm('refreshPageCallback');
    if ($form_state->hasAnyErrors()) {
      // If there are any form errors, re-display the form.
      $response = new AjaxResponse();
      $response->addCommand(new ReplaceCommand('.node-page-draft-form', $form));
      ksm('error');
      return $response;
    }
    
    $node = $form_state->getformObject()->getEntity();
    return $this->refreshPage($node);
  }


  /**
   * Helper function to refresh the field after an AJAX call.
   */
  public function refreshPage($node) {
    ksm('refreshPage');
    $identifier = 'html';
    // $client = \Drupal::httpClient();
    // $request = $client->get('http://development.localhost/node/'.$node->id());
    // $render = (string) $request->getBody();
    // $autoloader = require_once '/var/www/html/web/autoload.php';
    // $kernel = new DrupalKernel('prod', $autoloader);
    // $request = Request::create('/node/'.$node->id());
    // $request_response = $kernel->handle($request);
    //$html = $request_response->getContent();

    $autoloader = require('/var/www/html/vendor/autoload.php');
    ksm($autoloader);
    $kernel = new DrupalKernel('prod', $autoloader);
    //$http_kernel = \Drupal::service('http_kernel');
    $request = Request::create('/node/' . $node->id());
    $request_response = $kernel->handle($request);
    $html = $request_response->getContent();


    ksm($kernel);
    ksm($request_response);
    ksm($html);

    $response = new AjaxResponse();
    // Refresh the paragraphs field.
    $response->addCommand(new ReplaceCommand($identifier, $html));
    // $response->addCommand(new CloseDialogCommand('.modal'));
    // $response->addCommand(new InvokeCommand('body', 'removeClass', ['modal-open']));
    // $response->addCommand(new InvokeCommand('body', 'css', ['padding-right', '0']));
    // $response->addCommand(new InvokeCommand('body', 'css', ['padding-top', '0']));

    return $response;
  }


  /**
   * {@inheritdoc}
   */
  // public function save(array $form, FormStateInterface $form_state) {
  //   return \Drupal::service('vb_paragraphs.lineage.revisioner')->saveNewRevision($this->entity);
  // }

}
