<?php

/**
 * @file
 * Contains \Drupal\vb_content_moderation\EventSubscriber\DraftRedirectSubscriber
 */
 
namespace Drupal\vb_content_moderation\EventSubscriber;
 
use Drupal\Core\Url;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Drupal\vb_content_moderation\DraftOperations;
use Drupal\Core\Session\AccountInterface;


class DraftRedirectSubscriber implements EventSubscriberInterface {
 
  /**
   * The draft operations service.
   *
   * @var \Drupal\vb_content_moderation\DraftOperations
   */
  protected $draftOperations;

  /**
   * The current user account.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $currentUser;

  /**
   * @param PostService $postService
   */
  public function __construct(DraftOperations $draftOperations, AccountInterface $currentUser)
  {
      $this->draftOperations = $draftOperations;
      $this->currentUser = $currentUser;
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    // This announces which events you want to subscribe to.
    // We only need the request event for this example.  Pass
    // this an array of method names
    return([
      KernelEvents::REQUEST => [
        ['redirectDraftContent'],
      ]
    ]);
  }
 
  /**
   * Redirect requests for node detail pages to their respective draft.
   *
   * @param GetResponseEvent $event
   * @return void
   */
  public function redirectDraftContent(GetResponseEvent $event) {
    $request = $event->getRequest();
 
    // This is necessary because this also gets called on
    // node sub-tabs such as "edit", "revisions", etc.  This
    // prevents those pages from redirected.
    if ($request->attributes->get('_route') !== 'entity.node.canonical') {
      return;
    } else {
      $node = $request->attributes->get('node');
    }

    // Do not redirect users without edit permission
    if (!$node->access('update', $this->currentUser)) {
      return;
    }


    // Only redirect a node with a draft revision that isn't the only revision
    if (!$this->draftOperations->hasDraft($node)
      || $this->draftOperations->hasOnlyDraft($node)
    ) {
      return;
    }

    // This is where you set the destination.
    $response = new RedirectResponse(Url::fromRoute('<front>', [], ['absolute' => true])->toString() . '/node/' . $node->id() . '/latest', 302);

    $event->setResponse($response);
  }
}